# Ubooquity

[Ubooquity](https://vaemendis.net/ubooquity/) Ubooquity is a free home server for your comics and ebooks library

## Access

It is available at [https://ubooquity.{{ domain }}/](https://ubooquity.{{ domain }}/) or [http://ubooquity.{{ domain }}/](http://ubooquity.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://ubooquity.{{ tor_domain }}/](http://ubooquity.{{ tor_domain }}/)
{% endif %}
